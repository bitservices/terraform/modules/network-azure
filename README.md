<!---------------------------------------------------------------------------->

# network (azure)

<!---------------------------------------------------------------------------->

## Description

Manage [Azure] networking components such as [Virtual Network] and associated
resources.

<!---------------------------------------------------------------------------->

## Modules

* [private-endpoint](private-endpoint/README.md) - Manage private endpoint connections from [Azure] resources to a [Virtual Network].
* [subnet](subnet/README.md) - Provisions a subnet within an [Azure] [Virtual Network].
* [vnet](vnet/README.md) - Provisions an [Azure] [Virtual Network].

<!---------------------------------------------------------------------------->

[Azure]:           https://azure.microsoft.com/
[Virtual Network]: https://azure.microsoft.com/services/virtual-network/

<!---------------------------------------------------------------------------->
