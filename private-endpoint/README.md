<!---------------------------------------------------------------------------->

# private-endpoint

#### Manage private endpoint connections from [Azure] resources to a [Virtual Network]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/azure//private-endpoint`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"     { default = "terraform@bitservices.io" }
variable "company"   { default = "BITServices Ltd"          }
variable "location"  { default = "uksouth"                  }
variable "ipv4_cidr" { default = "10.100.0.0/16"            }
variable "ipv6_cidr" { default = "fd37:b175:5b87:9d00::/56" }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_vnet" {
  source                                = "gitlab.com/bitservices/network/azure//vnet"
  name                                  = "foobar1"
  cidrs                                 = tolist([ var.ipv6_cidr, var.ipv4_cidr ])
  group                                 = module.my_resource_group.name
  owner                                 = var.owner
  company                               = var.company
  location                              = var.location
  private_dns_link_storage_file_enabled = true
}

module "my_subnet" {
  source   = "gitlab.com/bitservices/network/azure//subnet"
  vnet     = module.my_vnet.name
  cidrs    = tolist([ cidrsubnet(var.ipv6_cidr, 8, 0), cidrsubnet(var.ipv4_cidr, 1, 0) ])
  class    = "backend"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_storage_account" {
  source   = "gitlab.com/bitservices/storage/azure//account"
  name     = "foobarsa1"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_storage_share" {
  source  = "gitlab.com/bitservices/storage/azure//share"
  account = module.my_storage_account.name
}

module "my_private_endpoint" {
  source                 = "gitlab.com/bitservices/network/azure//private-endpoint"
  vnet                   = module.my_vnet.name
  group                  = module.my_resource_group.name
  owner                  = var.owner
  company                = var.company
  location               = var.location
  resource_id            = module.storage_account.id
  resource_name          = module.storage_account.name
  resource_subresources  = [ "file" ]
  private_dns_zone_names = [ "privatelink.file.core.windows.net" ]
}
```

<!---------------------------------------------------------------------------->

[Azure]:           https://azure.microsoft.com/
[Virtual Network]: https://azure.microsoft.com/services/virtual-network/

<!---------------------------------------------------------------------------->
