###############################################################################
# Required Variables
###############################################################################

variable "vnet" {
  type        = string
  description = "The full name of the VNet this private endpoint will belong to."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this private endpoint."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this private endpoint."
}

###############################################################################

variable "resource_id" {
  type        = string
  description = "ID of target resource for the private endpoint connection."
}

variable "resource_name" {
  type        = string
  description = "Name of target resource for the private endpoint connection."
}

###############################################################################
# Optional Variables
###############################################################################

variable "resource_manual" {
  type        = bool
  default     = false
  description = "Does the Private Endpoint require Manual Approval from the remote resource owner?"
}

variable "resource_message" {
  type        = string
  default     = "Requested by Terraform"
  description = "A message passed to the owner of the remote resource when the private endpoint attempts to establish the connection to the remote resource. Ignored unless 'resource_manual' is 'true'."
}

variable "resource_subresources" {
  type        = list(string)
  default     = null
  description = "A list of subresource names which the Private Endpoint is able to connect to."
}

###############################################################################
# Locals
###############################################################################

locals {
  name             = format("%s-%s", var.vnet, var.resource_name)
  resource_message = var.resource_manual ? var.resource_message : null
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_endpoint" "object" {
  name                = local.name
  location            = var.location
  subnet_id           = data.azurerm_subnet.object.id
  resource_group_name = var.group

  tags = {
    "Name"         = local.name
    "VNet"         = var.vnet
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Resource"     = var.resource_name
    "Subscription" = data.azurerm_subscription.current.display_name
  }

  private_dns_zone_group {
    name                 = local.name
    private_dns_zone_ids = tolist(data.azurerm_private_dns_zone.object.*.id)
  }

  private_service_connection {
    name                           = local.name
    request_message                = local.resource_message
    subresource_names              = var.resource_subresources
    is_manual_connection           = var.resource_manual
    private_connection_resource_id = var.resource_id
  }
}

###############################################################################
# Outputs
###############################################################################

output "vnet" {
  value = var.vnet
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "resource_id" {
  value = var.resource_id
}

output "resource_name" {
  value = var.resource_name
}

###############################################################################

output "resource_manual" {
  value = var.resource_manual
}

output "resource_message" {
  value = local.resource_message
}

output "resource_subresources" {
  value = var.resource_subresources
}

###############################################################################

output "id" {
  value = azurerm_private_endpoint.object.id
}

output "name" {
  value = azurerm_private_endpoint.object.name
}

output "location" {
  value = azurerm_private_endpoint.object.location
}

###############################################################################
