###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_zone_names" {
  type        = list(string)
  description = "The name of the private DNS zone for the private endpoint."
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_private_dns_zone" "object" {
  count               = length(var.private_dns_zone_names)
  name                = var.private_dns_zone_names[count.index]
  resource_group_name = var.group
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_ids" {
  value = data.azurerm_private_dns_zone.object.*.id
}

output "private_dns_names" {
  value = data.azurerm_private_dns_zone.object.*.name
}

output "private_dns_tags" {
  value = data.azurerm_private_dns_zone.object.*.tags
}

output "private_dns_record_sets" {
  value = data.azurerm_private_dns_zone.object.*.number_of_record_sets
}

output "private_dns_max_vnet_links" {
  value = data.azurerm_private_dns_zone.object.*.max_number_of_virtual_network_links
}

output "private_dns_max_record_sets" {
  value = data.azurerm_private_dns_zone.object.*.max_number_of_record_sets
}

output "private_dns_max_registered_vnet_links" {
  value = data.azurerm_private_dns_zone.object.*.max_number_of_virtual_network_links_with_registration
}

###############################################################################
