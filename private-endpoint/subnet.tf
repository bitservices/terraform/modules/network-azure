###############################################################################
# Optional Variables
###############################################################################

variable "subnet_class" {
  type        = string
  default     = "backend"
  description = "Identifier for the target subnet within its VNET."
}

###############################################################################
# Locals
###############################################################################

locals {
  subnet_name = format("%s-%s", var.vnet, var.subnet_class)
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_subnet" "object" {
  name                 = local.subnet_name
  resource_group_name  = var.group
  virtual_network_name = var.vnet
}

###############################################################################
# Outputs
###############################################################################

output "subnet_class" {
  value = var.subnet_class
}

###############################################################################

output "subnet_id" {
  value = data.azurerm_subnet.object.id
}

output "subnet_name" {
  value = data.azurerm_subnet.object.name
}

output "subnet_endpoints" {
  value = data.azurerm_subnet.object.service_endpoints
}

output "subnet_ipv4_cidr" {
  value = data.azurerm_subnet.object.address_prefixes
}

output "subnet_route_table_id" {
  value = data.azurerm_subnet.object.route_table_id
}

output "subnet_security_group_id" {
  value = data.azurerm_subnet.object.network_security_group_id
}

output "subnet_private_link_services_enabled" {
  value = data.azurerm_subnet.object.enforce_private_link_service_network_policies
}

output "subnet_private_link_endpoints_enabled" {
  value = data.azurerm_subnet.object.enforce_private_link_endpoint_network_policies
}

###############################################################################
