<!---------------------------------------------------------------------------->

# subnet

#### Provisions a subnet within an [Azure] [Virtual Network]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/azure//subnet`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"     { default = "terraform@bitservices.io" }
variable "company"   { default = "BITServices Ltd"          }
variable "location"  { default = "uksouth"                  }
variable "ipv4_cidr" { default = "10.100.0.0/16"            }
variable "ipv6_cidr" { default = "fd37:b175:5b87:9d00::/56" }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_vnet" {
  source   = "gitlab.com/bitservices/network/azure//vnet"
  name     = "foobar1"
  cidrs    = tolist([ var.ipv6_cidr, var.ipv4_cidr ])
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_subnet" {
  source   = "gitlab.com/bitservices/network/azure//subnet"
  vnet     = module.my_vnet.name
  cidrs    = tolist([ cidrsubnet(var.ipv6_cidr, 8, 0), cidrsubnet(var.ipv4_cidr, 1, 0) ])
  class    = "backend"
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}
```

<!---------------------------------------------------------------------------->

[Azure]:           https://azure.microsoft.com/
[Terraform]:       https://www.terraform.io/
[Virtual Network]: https://azure.microsoft.com/services/virtual-network/

<!---------------------------------------------------------------------------->
