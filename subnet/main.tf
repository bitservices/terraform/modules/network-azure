###############################################################################
# Required Variables
###############################################################################

variable "vnet" {
  type        = string
  description = "The full name of the VNet this subnet will belong to."
}

variable "cidrs" {
  type        = list(string)
  description = "The IPv6 and/or IPv4 address space that is used the virtual network. You can supply more than one CIDR."

  validation {
    condition     = length(compact(var.cidrs)) >= 1
    error_message = "At least one IPv4 or IPv6 CIDR range must be provided."
  }
}

variable "class" {
  type        = string
  description = "Identifier for this subnet within its VNET."
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain the network security group associated with this subnet."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this resource group."
}

###############################################################################
# Optional Variables
###############################################################################

variable "endpoints" {
  type        = list(string)
  default     = null
  description = "The list of Service endpoints to associate with the subnet. Possible values include: Microsoft.AzureActiveDirectory, Microsoft.AzureCosmosDB, Microsoft.ContainerRegistry, Microsoft.EventHub, Microsoft.KeyVault, Microsoft.ServiceBus, Microsoft.Sql, Microsoft.Storage and Microsoft.Web."
}

variable "private_endpoints_enabled" {
  type        = bool
  default     = false
  description = "Enable or Disable network policies for the private link endpoint on the subnet."
}

variable "private_link_services_enabled" {
  type        = bool
  default     = false
  description = "Enable or Disable network policies for the private link service on the subnet."
}

###############################################################################

variable "delegations" {
  type = list(object({
    name    = string
    service = object({
      name    = string
      actions = optional(list(string))
    })
  }))
  default     = []
  description = "A list of Azure resource types this subnet is delegated to."
}

###############################################################################
# Local Variables
###############################################################################

locals {
  name                                          = format("%s-%s", var.vnet, var.class)
  private_endpoint_network_policies_enabled     = var.private_endpoints_enabled ? false : true
  private_link_service_network_policies_enabled = var.private_link_services_enabled ? false : true
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_subnet" "object" {
  name                                           = local.name
  address_prefixes                               = var.cidrs
  service_endpoints                              = var.endpoints
  resource_group_name                            = var.group
  virtual_network_name                           = var.vnet
  enforce_private_link_service_network_policies  = local.private_endpoint_network_policies_enabled
  enforce_private_link_endpoint_network_policies = local.private_link_service_network_policies_enabled

  dynamic "delegation" {
    for_each = var.delegations

    content {
      name = delegation.value.name

      service_delegation {
        name    = delegation.value.service.name
        actions = delegation.value.service.actions
      }
    }
  }
}

###############################################################################
# Outputs
###############################################################################

output "vnet" {
  value = var.vnet
}

output "class" {
  value = var.class
}

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

output "location" {
  value = var.location
}

###############################################################################

output "endpoints" {
  value = var.endpoints
}

output "private_endpoints_enabled" {
  value = var.private_endpoints_enabled
}

output "private_link_services_enabled" {
  value = var.private_link_services_enabled
}

output "private_endpoint_network_policies_enabled" {
 value = local.private_endpoint_network_policies_enabled
}

output "private_link_service_network_policies_enabled" {
  value = local.private_link_service_network_policies_enabled
}

###############################################################################

output "delegations" {
  value = var.delegations
}

###############################################################################

output "id" {
  value = azurerm_subnet.object.id

  depends_on = [
    azurerm_network_security_rule.object,
    azurerm_subnet_nat_gateway_association.object,
    azurerm_subnet_network_security_group_association.object
  ]
}

output "name" {
  value = azurerm_subnet.object.name

  depends_on = [
    azurerm_network_security_rule.object,
    azurerm_subnet_nat_gateway_association.object,
    azurerm_subnet_network_security_group_association.object
  ]
}

output "cidrs" {
  value = azurerm_subnet.object.address_prefixes
}

###############################################################################
