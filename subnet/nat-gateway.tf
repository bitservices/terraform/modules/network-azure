###############################################################################
# Optional Variables
###############################################################################

variable "nat_gateway_id" {
  type        = string
  default     = null
  description = "ID of the NAT gateway that should be attached to this subnet. Ignored if 'nat_gateway_enabled' is 'false' or 'nat_gateway_lookup' is 'true'."
}

variable "nat_gateway_class" {
  type        = string
  default     = "nat"
  description = "Identifier for which NAT gateway should be attached to this subnet. This is appended to the VNET name. Ignored if 'nat_gateway_enabled' is 'false' or 'nat_gateway_id' is set."
}

variable "nat_gateway_lookup" {
  type        = bool
  default     = false
  description = "Lookup NAT gateway based on 'nat_gateway_class'. Must be 'true' if 'nat_gateway_id' is not set and 'nat_gateway_enabled' is 'true'."
}

variable "nat_gateway_enabled" {
  type        = bool
  default     = false
  description = "Should the VNET NAT gateway be attached to this subnet?"
}

###############################################################################
# Local Variables
###############################################################################

locals {
  nat_gateway_id     = local.nat_gateway_lookup ? data.azurerm_nat_gateway.object[0].id : var.nat_gateway_id
  nat_gateway_name   = format("%s-%s", var.vnet, var.nat_gateway_class)
  nat_gateway_lookup = var.nat_gateway_enabled ? var.nat_gateway_lookup : false
}

###############################################################################
# Data Sources
###############################################################################

data azurerm_nat_gateway "object" {
  count               = local.nat_gateway_lookup ? 1 : 0
  name                = local.nat_gateway_name
  resource_group_name = var.group
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_subnet_nat_gateway_association" "object" {
  count          = var.nat_gateway_enabled ? 1 : 0
  subnet_id      = azurerm_subnet.object.id
  nat_gateway_id = local.nat_gateway_id
}

###############################################################################
# Outputs
###############################################################################

output "nat_gateway_id" {
  value = local.nat_gateway_id
}

output "nat_gateway_class" {
  value = var.nat_gateway_class
}

output "nat_gateway_lookup" {
  value = local.nat_gateway_lookup
}

output "nat_gateway_enabled" {
  value = var.nat_gateway_enabled
}

###############################################################################

output "nat_gateway_association_id" {
  value = length(azurerm_subnet_nat_gateway_association.object) == 1 ? azurerm_subnet_nat_gateway_association.object[0].id : null
}

###############################################################################
