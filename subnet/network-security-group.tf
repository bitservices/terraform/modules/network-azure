###############################################################################
# Optional Variables
###############################################################################

variable "security_group_default_description" {
  type        = string
  default     = "Managed by Terraform"
  description = "Default description for network security group rules for the network security group."
}

###############################################################################

variable "security_group_enable_vnet_in" {
  type        = bool
  default     = false
  description = "Should built-in rule for communication from the VNET be enabled?"
}

variable "security_group_enable_vnet_out" {
  type        = bool
  default     = false
  description = "Should built-in rule for communication to the VNET be enabled?"
}

variable "security_group_enable_internet_out" {
  type        = bool
  default     = false
  description = "Should built-in rule for outbound communication to the Internet be enabled?"
}

variable "security_group_enable_load_balancer" {
  type        = bool
  default     = false
  description = "Should built-in rule for communication from the Azure Load Balancer be enabled?"
}

###############################################################################

variable "security_group_rules_core" {
  type = list(object({
    name                                       = string
    access                                     = string
    protocol                                   = string
    priority                                   = number
    direction                                  = string
    description                                = optional(string)
    source_cidr                                = optional(string)
    source_cidrs                               = optional(set(string))
    destination_cidr                           = optional(string)
    destination_cidrs                          = optional(set(string))
    source_port_range                          = optional(string)
    source_port_ranges                         = optional(set(string))
    destination_port_range                     = optional(string)
    destination_port_ranges                    = optional(set(string))
    source_application_security_group_ids      = optional(set(string))
    destination_application_security_group_ids = optional(set(string))
  }))
  default = [
    {
      "name"                   = "DenyAllInbound"
      "access"                 = "Deny"
      "protocol"               = "*"
      "priority"               = 4096
      "direction"              = "Inbound"
      "source_cidr"            = "*"
      "destination_cidr"       = "*"
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    },
    {
      "name"                   = "DenyAllOutbound"
      "access"                 = "Deny"
      "protocol"               = "*"
      "priority"               = 4096
      "direction"              = "Outbound"
      "source_cidr"            = "*"
      "destination_cidr"       = "*"
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    }
  ]
  description = "The core list of network security rules for the network security group."
}

variable "security_group_rules_extra" {
  type = list(object({
    name                                       = string
    access                                     = string
    protocol                                   = string
    priority                                   = number
    direction                                  = string
    description                                = optional(string)
    source_cidr                                = optional(string)
    source_cidrs                               = optional(set(string))
    destination_cidr                           = optional(string)
    destination_cidrs                          = optional(set(string))
    source_port_range                          = optional(string)
    source_port_ranges                         = optional(set(string))
    destination_port_range                     = optional(string)
    destination_port_ranges                    = optional(set(string))
    source_application_security_group_ids      = optional(set(string))
    destination_application_security_group_ids = optional(set(string))
  }))
  default     = []
  description = "A user supplied list of network security rules for the network security group."
}

variable "security_group_rules_vnet_in" {
  type = list(object({
    name                                       = string
    access                                     = string
    protocol                                   = string
    priority                                   = number
    direction                                  = string
    description                                = optional(string)
    source_cidr                                = optional(string)
    source_cidrs                               = optional(set(string))
    destination_cidr                           = optional(string)
    destination_cidrs                          = optional(set(string))
    source_port_range                          = optional(string)
    source_port_ranges                         = optional(set(string))
    destination_port_range                     = optional(string)
    destination_port_ranges                    = optional(set(string))
    source_application_security_group_ids      = optional(set(string))
    destination_application_security_group_ids = optional(set(string))
  }))
  default = [
    {
      "name"                   = "AllowVnetInbound"
      "access"                 = "Allow"
      "protocol"               = "*"
      "priority"               = 4000
      "direction"              = "Inbound"
      "source_cidr"            = "VirtualNetwork"
      "destination_cidr"       = "VirtualNetwork"
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    }
  ]
  description = "The built-in rule for communication from the VNET."
}

variable "security_group_rules_vnet_out" {
  type = list(object({
    name                                       = string
    access                                     = string
    protocol                                   = string
    priority                                   = number
    direction                                  = string
    description                                = optional(string)
    source_cidr                                = optional(string)
    source_cidrs                               = optional(set(string))
    destination_cidr                           = optional(string)
    destination_cidrs                          = optional(set(string))
    source_port_range                          = optional(string)
    source_port_ranges                         = optional(set(string))
    destination_port_range                     = optional(string)
    destination_port_ranges                    = optional(set(string))
    source_application_security_group_ids      = optional(set(string))
    destination_application_security_group_ids = optional(set(string))
  }))
  default = [
    {
      "name"                   = "AllowVnetOutbound"
      "access"                 = "Allow"
      "protocol"               = "*"
      "priority"               = 4000
      "direction"              = "Outbound"
      "source_cidr"            = "VirtualNetwork"
      "destination_cidr"       = "VirtualNetwork"
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    }
  ]
  description = "The built-in rule for communication to the VNET."
}

variable "security_group_rules_internet_out" {
  type = list(object({
    name                                       = string
    access                                     = string
    protocol                                   = string
    priority                                   = number
    direction                                  = string
    description                                = optional(string)
    source_cidr                                = optional(string)
    source_cidrs                               = optional(set(string))
    destination_cidr                           = optional(string)
    destination_cidrs                          = optional(set(string))
    source_port_range                          = optional(string)
    source_port_ranges                         = optional(set(string))
    destination_port_range                     = optional(string)
    destination_port_ranges                    = optional(set(string))
    source_application_security_group_ids      = optional(set(string))
    destination_application_security_group_ids = optional(set(string))
  }))
  default = [
    {
      "name"                   = "AllowInternetOutbound"
      "access"                 = "Allow"
      "protocol"               = "*"
      "priority"               = 4001
      "direction"              = "Outbound"
      "source_cidr"            = "VirtualNetwork"
      "destination_cidr"       = "Internet"
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    }
  ]
  description = "The built-in rule for outbound communication to the Internet."
}

variable "security_group_rules_load_balancer" {
  type = list(object({
    name                                       = string
    access                                     = string
    protocol                                   = string
    priority                                   = number
    direction                                  = string
    description                                = optional(string)
    source_cidr                                = optional(string)
    source_cidrs                               = optional(set(string))
    source_port_range                          = optional(string)
    source_port_ranges                         = optional(set(string))
    destination_cidr                           = optional(string)
    destination_cidrs                          = optional(set(string))
    destination_port_range                     = optional(string)
    destination_port_ranges                    = optional(set(string))
    source_application_security_group_ids      = optional(set(string))
    destination_application_security_group_ids = optional(set(string))
  }))
  default = [
    {
      "name"                   = "AllowAzureLoadBalancerInBound"
      "access"                 = "Allow"
      "protocol"               = "*"
      "priority"               = 4001
      "direction"              = "Inbound"
      "source_cidr"            = "AzureLoadBalancer"
      "destination_cidr"       = "VirtualNetwork"
      "source_port_range"      = "*"
      "destination_port_range" = "*"
    }
  ]
  description = "The built-in rule for communication from the Azure Load Balancer."
}

###############################################################################
# Locals
###############################################################################

locals {
  security_group_rules = concat(local.security_group_rules_core, local.security_group_rules_extra, local.security_group_rules_vnet_in, local.security_group_rules_vnet_out, local.security_group_rules_internet_out, local.security_group_rules_load_balancer)

  security_group_rules_core = defaults(var.security_group_rules_core, {
    "description" = var.security_group_default_description
  })

  security_group_rules_extra = defaults(var.security_group_rules_extra, {
    "description" = var.security_group_default_description
  })

  security_group_rules_vnet_in = var.security_group_enable_vnet_in ? defaults(var.security_group_rules_vnet_in, {
    "description" = var.security_group_default_description
  }) : []

  security_group_rules_vnet_out = var.security_group_enable_vnet_out ? defaults(var.security_group_rules_vnet_out, {
    "description" = var.security_group_default_description
  }) : []

  security_group_rules_internet_out = var.security_group_enable_internet_out ? defaults(var.security_group_rules_internet_out, {
    "description" = var.security_group_default_description
  }) : []

  security_group_rules_load_balancer = var.security_group_enable_load_balancer ? defaults(var.security_group_rules_load_balancer, {
    "description" = var.security_group_default_description
  }) : []
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_network_security_group" "object" {
  name                = azurerm_subnet.object.name
  location            = var.location
  resource_group_name = var.group

  tags = {
    "Name"         = azurerm_subnet.object.name
    "VNet"         = var.vnet
    "Class"        = var.class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################

resource "azurerm_network_security_rule" "object" {
  for_each = {
    for rule in local.security_group_rules: rule.name => rule
  }

  name                                       = each.value["name"]
  access                                     = each.value["access"]
  protocol                                   = each.value["protocol"]
  priority                                   = each.value["priority"]
  direction                                  = each.value["direction"]
  description                                = each.value["description"]
  source_port_range                          = each.value["source_port_range"]
  source_port_ranges                         = length(each.value["source_port_ranges"]) == 0 ? null : each.value["source_port_ranges"]
  resource_group_name                        = var.group
  source_address_prefix                      = each.value["source_cidr"]
  destination_port_range                     = each.value["destination_port_range"]
  destination_port_ranges                    = length(each.value["destination_port_ranges"]) == 0 ? null : each.value["destination_port_ranges"]
  source_address_prefixes                    = length(each.value["source_cidrs"]) == 0 ? null : each.value["source_cidrs"]
  destination_address_prefix                 = each.value["destination_cidr"]
  network_security_group_name                = azurerm_network_security_group.object.name
  destination_address_prefixes               = length(each.value["destination_cidrs"]) == 0 ? null : each.value["destination_cidrs"]
  source_application_security_group_ids      = length(each.value["source_application_security_group_ids"]) == 0 ? null : each.value["source_application_security_group_ids"]
  destination_application_security_group_ids = length(each.value["destination_application_security_group_ids"]) == 0 ? null : each.value["destination_application_security_group_ids"]
}

###############################################################################

resource "azurerm_subnet_network_security_group_association" "object" {
  subnet_id                 = azurerm_subnet.object.id
  network_security_group_id = azurerm_network_security_group.object.id
}

###############################################################################
# Outputs
###############################################################################

output "security_group_default_description" {
  value = var.security_group_default_description
}

###############################################################################

output "security_group_enable_vnet_in" {
  value = var.security_group_enable_vnet_in
}

output "security_group_enable_vnet_out" {
  value = var.security_group_enable_vnet_out
}

output "security_group_enable_internet_out" {
  value = var.security_group_enable_internet_out
}

output "security_group_enable_load_balancer" {
  value = var.security_group_enable_load_balancer
}

###############################################################################

output "security_group_rules" {
  value = local.security_group_rules
}

output "security_group_rules_core" {
  value = local.security_group_rules_core
}

output "security_group_rules_extra" {
  value = local.security_group_rules_extra
}

output "security_group_rules_vnet_in" {
  value = local.security_group_rules_vnet_in
}

output "security_group_rules_vnet_out" {
  value = local.security_group_rules_vnet_out
}

output "security_group_rules_internet_out" {
  value = local.security_group_rules_internet_out
}

output "security_group_rules_load_balancer" {
  value = local.security_group_rules_load_balancer
}

###############################################################################

output "network_security_group_id" {
  value = azurerm_network_security_group.object.id
}

output "network_security_group_name" {
  value = azurerm_network_security_group.object.name
}

###############################################################################

output "network_security_group_rules_ids" {
  value = [
    for key, value in azurerm_network_security_rule.object : value.id
  ]
}

###############################################################################

output "network_security_group_association_id" {
  value = azurerm_subnet_network_security_group_association.object.id
}

###############################################################################
