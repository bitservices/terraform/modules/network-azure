<!---------------------------------------------------------------------------->

# vnet

#### Provisions an [Azure] [Virtual Network]

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/network/azure//vnet`**

-------------------------------------------------------------------------------

### Example Usage

```
variable "owner"    { default = "terraform@bitservices.io" }
variable "company"  { default = "BITServices Ltd"          }
variable "location" { default = "uksouth"                  }

module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = var.owner
  company  = var.company
  location = var.location
}

module "my_vnet" {
  source   = "gitlab.com/bitservices/network/azure//vnet"
  name     = "foobar1"
  cidrs    = tolist([ "fd37:b175:5b87:9d00::/56", "10.100.0.0/16" ])
  group    = module.my_resource_group.name
  owner    = var.owner
  company  = var.company
  location = var.location
}
```

<!---------------------------------------------------------------------------->

[Azure]:           https://azure.microsoft.com/
[Virtual Network]: https://azure.microsoft.com/services/virtual-network/

<!---------------------------------------------------------------------------->
