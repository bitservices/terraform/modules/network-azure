###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "This VNETs unique name within the resource group."
}

variable "cidrs" {
  type        = list(string)
  description = "The IPv4 and/or IPv6 address space that is used the virtual network. You can supply more than one CIDR."

  validation {
    condition     = length(compact(var.cidrs)) >= 1
    error_message = "At least one IPv6 or IPv4 CIDR range must be provided."
  }
}

variable "group" {
  type        = string
  description = "The full name of the resource group that will contain this virtual network."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this virtual network."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_virtual_network" "object" {
  name                = var.name
  location            = var.location
  address_space       = var.cidrs
  resource_group_name = var.group

  tags = {
    "Name"         = var.name
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################

output "group" {
  value = var.group
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "id" {
  value = azurerm_virtual_network.object.id
}

output "name" {
  value = azurerm_virtual_network.object.name
}

output "cidrs" {
  value = azurerm_virtual_network.object.address_space
}

output "location" {
  value = azurerm_virtual_network.object.location
}

###############################################################################
