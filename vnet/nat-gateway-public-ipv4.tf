###############################################################################
# Optional Variables
###############################################################################

variable "nat_gateway_public_ipv4_sku" {
  type        = string
  default     = "Standard"
  description = "The SKU of the Public IPv4 addresses to assign to the NAT gateway."
}

variable "nat_gateway_public_ipv4_count" {
  type        = number
  default     = 1
  description = "Number of public IPv4 addresses to assign to the NAT gateway."

  validation {
    condition     = var.nat_gateway_public_ipv4_count >= 1 && var.nat_gateway_public_ipv4_count <= 16
    error_message = "The NAT gateway public IPv4 address count must be between 1 and 16."
  }
}

variable "nat_gateway_public_ipv4_zones" {
  type        = list(number)
  default     = null
  description = "Availability zones for the public IPv4 addresses to assign to the NAT gateway. Default is none (not zone redundant)."
}

variable "nat_gateway_public_ipv4_version" {
  type        = string
  default     = "IPv4"
  description = "The version tag for IPv4. This should never be changed."
}

variable "nat_gateway_public_ipv4_allocation" {
  type        = string
  default     = "Static"
  description = "Defines the allocation method for the NAT gateway IPv4 addresses. Possible values are Static or Dynamic. Ignored if 'nat_gateway_public_ipv4_sku' is 'Standard'."
}

###############################################################################
# Local Variables
###############################################################################

locals {
  nat_gateway_public_ipv4_class      = format("%s-%s-%s", var.name, var.nat_gateway_class, lower(var.nat_gateway_public_ipv4_version))
  nat_gateway_public_ipv4_count      = var.nat_gateway_enabled ? var.nat_gateway_public_ipv4_count : 0
  nat_gateway_public_ipv4_allocation = var.nat_gateway_public_ipv4_sku == "Standard" ? "Static" : var.nat_gateway_public_ipv4_allocation
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_public_ip" "ipv4" {
  count                   = local.nat_gateway_public_ipv4_count
  sku                     = var.nat_gateway_public_ipv4_sku
  name                    = format("%s-%02d", local.nat_gateway_public_ipv4_class, count.index)
  zones                   = var.nat_gateway_public_ipv4_zones
  location                = var.location
  allocation_method       = local.nat_gateway_public_ipv4_allocation
  resource_group_name     = var.group
  idle_timeout_in_minutes = var.nat_gateway_timeout

  tags = {
    "SKU"          = var.nat_gateway_public_ipv4_sku
    "Name"         = format("%s-%02d", local.nat_gateway_public_ipv4_class, count.index)
    "VNet"         = var.name
    "Class"        = local.nat_gateway_public_ipv4_class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Version"      = var.nat_gateway_public_ipv4_version
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################

resource "azurerm_nat_gateway_public_ip_association" "ipv4" {
  count                = length(azurerm_public_ip.ipv4)
  nat_gateway_id       = azurerm_nat_gateway.object[0].id
  public_ip_address_id = azurerm_public_ip.ipv4[count.index].id
}

###############################################################################
# Outputs
###############################################################################

output "nat_gateway_public_ipv4_sku" {
  value = var.nat_gateway_public_ipv4_sku
}

output "nat_gateway_public_ipv4_class" {
  value = local.nat_gateway_public_ipv4_class
}

output "nat_gateway_public_ipv4_count" {
  value = local.nat_gateway_public_ipv4_count
}

output "nat_gateway_public_ipv4_zones" {
  value = var.nat_gateway_public_ipv4_zones
}

output "nat_gateway_public_ipv4_version" {
  value = var.nat_gateway_public_ipv4_version
}

output "nat_gateway_public_ipv4_allocation" {
  value = local.nat_gateway_public_ipv4_allocation
}

###############################################################################

output "nat_gateway_public_ipv4_ids" {
  value = azurerm_public_ip.ipv4[*].id
}

output "nat_gateway_public_ipv4_fqdns" {
  value = azurerm_public_ip.ipv4[*].fqdn
}

output "nat_gateway_public_ipv4_names" {
  value = azurerm_public_ip.ipv4[*].name
}

output "nat_gateway_public_ipv4_addresses" {
  value = azurerm_public_ip.ipv4[*].ip_address
}

###############################################################################

output "nat_gateway_public_ipv4_association_ids" {
  value = azurerm_nat_gateway_public_ip_association.ipv4[*].id
}

###############################################################################
