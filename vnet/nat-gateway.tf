###############################################################################
# Optional Variables
###############################################################################

variable "nat_gateway_sku" {
  type        = string
  default     = "Standard"
  description = "The SKU which should be used for the NAT gateway."
}

variable "nat_gateway_class" {
  type        = string
  default     = "nat"
  description = "Identifier for the NAT gateway and associated public IP addresses. This is appended to the VNET name."
}

variable "nat_gateway_enabled" {
  type        = bool
  default     = false
  description = "Should a NAT gateway be created for this VNET."
}

variable "nat_gateway_timeout" {
  type        = number
  default     = 5
  description = "The idle timeout which should be used in minutes."
}

###############################################################################
# Local Variables
###############################################################################

locals {
  nat_gateway_name = format("%s-%s", var.name, var.nat_gateway_class)
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_nat_gateway" "object" {
  count                   = var.nat_gateway_enabled ? 1 : 0
  name                    = local.nat_gateway_name
  location                = var.location
  sku_name                = var.nat_gateway_sku
  resource_group_name     = var.group
  idle_timeout_in_minutes = var.nat_gateway_timeout

  tags = {
    "SKU"          = var.nat_gateway_sku
    "Name"         = local.nat_gateway_name
    "VNet"         = var.name
    "Class"        = var.nat_gateway_class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "nat_gateway_sku" {
  value = var.nat_gateway_sku
}

output "nat_gateway_class" {
  value = var.nat_gateway_class
}

output "nat_gateway_enabled" {
  value = var.nat_gateway_enabled
}

output "nat_gateway_timeout" {
  value = var.nat_gateway_timeout
}

###############################################################################

output "nat_gateway_id" {
  value = length(azurerm_nat_gateway.object) == 1 ? azurerm_nat_gateway.object[0].id : null

  depends_on = [
    azurerm_nat_gateway_public_ip_association.ipv4
  ]
}

output "nat_gateway_guid" {
  value = length(azurerm_nat_gateway.object) == 1 ? azurerm_nat_gateway.object[0].resource_guid : null

  depends_on = [
    azurerm_nat_gateway_public_ip_association.ipv4
  ]
}

output "nat_gateway_name" {
  value = length(azurerm_nat_gateway.object) == 1 ? azurerm_nat_gateway.object[0].name : null

  depends_on = [
    azurerm_nat_gateway_public_ip_association.ipv4
  ]
}

###############################################################################
