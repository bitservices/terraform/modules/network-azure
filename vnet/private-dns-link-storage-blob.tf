###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_link_storage_blob_class" {
  type        = string
  default     = "storage-blob"
  description = "The private DNS zone VNet link suffix for storage account blob container private link connections. This is appended to the VNet name."
}

variable "private_dns_link_storage_blob_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone link be created for storage account blob container connections."
}

variable "private_dns_link_storage_blob_zone_name" {
  type        = string
  default     = "privatelink.blob.core.windows.net"
  description = "The name of the private DNS zone for storage account blob container private link connections."
}

variable "private_dns_link_storage_blob_registration_enabled" {
  type        = bool
  default     = false
  description = "Is auto-registration of virtual machine records in the virtual network in the Private DNS zone enabled? Should always be 'false'."
}

###############################################################################
# Locals
###############################################################################

locals {
  private_dns_link_storage_blob_name = format("%s-%s", azurerm_virtual_network.object.name, var.private_dns_link_storage_blob_class)
}

###############################################################################
# Data Sources
###############################################################################

data "azurerm_private_dns_zone" "storage_blob" {
  count               = var.private_dns_link_storage_blob_enabled ? 1 : 0
  name                = var.private_dns_link_storage_blob_zone_name
  resource_group_name = var.group
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone_virtual_network_link" "storage_blob" {
  count                 = var.private_dns_link_storage_blob_enabled ? 1 : 0
  name                  = local.private_dns_link_storage_blob_name
  virtual_network_id    = azurerm_virtual_network.object.id
  resource_group_name   = var.group
  registration_enabled  = var.private_dns_link_storage_blob_registration_enabled
  private_dns_zone_name = data.azurerm_private_dns_zone.storage_blob[0].name

  tags = {
    "Name"         = local.private_dns_link_storage_blob_name
    "VNet"         = azurerm_virtual_network.object.name
    "Zone"         = data.azurerm_private_dns_zone.storage_blob[0].name
    "Class"        = var.private_dns_link_storage_blob_class
    "Group"        = var.group
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Registration" = var.private_dns_link_storage_blob_registration_enabled ? "Yes" : "No"
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_link_storage_blob_class" {
  value = var.private_dns_link_storage_blob_class
}

output "private_dns_link_storage_blob_enabled" {
  value = var.private_dns_link_storage_blob_enabled
}

output "private_dns_link_storage_blob_registration_enabled" {
  value = var.private_dns_link_storage_blob_registration_enabled
}

###############################################################################

output "private_dns_link_storage_blob_zone_id" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].id : null
}

output "private_dns_link_storage_blob_zone_name" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].name : null
}

output "private_dns_link_storage_blob_zone_tags" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].tags : null
}

output "private_dns_link_storage_blob_zone_record_max" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].max_number_of_record_sets : null
}

output "private_dns_link_storage_blob_zone_record_count" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].number_of_record_sets : null
}

output "private_dns_link_storage_blob_zone_vnet_link_max" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].max_number_of_virtual_network_links : null
}

output "private_dns_link_storage_blob_zone_vnet_registration_link_max" {
  value = length(data.azurerm_private_dns_zone.storage_blob) == 1 ? data.azurerm_private_dns_zone.storage_blob[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################

output "private_dns_link_storage_blob_id" {
  value = length(azurerm_private_dns_zone_virtual_network_link.storage_blob) == 1 ? azurerm_private_dns_zone_virtual_network_link.storage_blob[0].id : null
}

output "private_dns_link_storage_blob_name" {
  value = length(azurerm_private_dns_zone_virtual_network_link.storage_blob) == 1 ? azurerm_private_dns_zone_virtual_network_link.storage_blob[0].name : null
}

###############################################################################
